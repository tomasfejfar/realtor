<?php
declare(strict_types = 1);

namespace App\Servers;

use App\Estate\Estate;

interface DownloadedPageParser
{
    /**
     * @param DownloadedPage $downloadedPage
     * @return Estate[]
     */
    public function parse(DownloadedPage $downloadedPage): array;
}
