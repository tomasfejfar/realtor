<?php
declare(strict_types = 1);

namespace App\Servers\Sreality;

use App\Estate\Estate;
use App\Servers\DownloadedPage;
use App\Servers\DownloadedPageParser;
use Consistence\Type\ArrayType\ArrayType;

class SrealityParser implements DownloadedPageParser
{
    /**
     * @param DownloadedPage $downloadedPage
     * @return Estate[]
     */
    public function parse(DownloadedPage $downloadedPage): array
    {
        $data = \GuzzleHttp\json_decode($downloadedPage->getContent(), true);

        return ArrayType::mapValuesByCallback(
            $data['_embedded']['estates'], function ($estate) {
            return new Estate(
                (string)$estate['hash_id'],
                $estate['name'],
                $estate['locality'],
                $this->buildUrl(
                    $estate['seo']['category_main_cb'],
                    $estate['seo']['category_sub_cb'],
                    $estate['seo']['category_type_cb'],
                    $estate['seo']['locality'],
                    (string) $estate['hash_id']
                )
            );
        });
    }

    private function buildUrl(
        int $category_main_cb,
        int $category_sub_cb,
        int $category_type_cb,
        string $locality,
        string $hashId
    )
    {
        /*

        category_main_cb:2
        category_sub_cb:37
        category_type_cb:1
        locality:"jablonec-nad-nisou-jablonec-nad-nisou-svatopluka-cecha"
         ==> /detail/prodej/dum/rodinny/jablonec-nad-nisou-jablonec-nad-nisou-svatopluka-cecha/4285362524
        ==> /detail/category_type_cb_detail/category_main_cb_detail/category_sub_cb_detail/locality

         */
        // from https://www.sreality.cz/js/all.js
        $codebooks = \GuzzleHttp\json_decode(
            '{"category_type_cb_detail":{"0":"vse","1":"prodej","2":"pronajem","3":"drazby"},"category_main_cb":{"0":"vse","1":"byty","2":"domy","3":"pozemky","4":"komercni","5":"ostatni"},"category_sub_cb":{"2":"1+kk","3":"1+1","4":"2+kk","5":"2+1","6":"3+kk","7":"3+1","8":"4+kk","9":"4+1","10":"5+kk","11":"5+1","12":"6-a-vice","16":"atypicky","47":"pokoj","37":"rodinne-domy","39":"vily","43":"chalupy","33":"chaty","35":"pamatky-jine","40":"projekty-na-klic","44":"zemedelske-usedlosti","19":"stavebni-parcely","18":"komercni-pozemky","20":"pole","22":"louky","21":"lesy","46":"rybniky","48":"sady-vinice","23":"zahrady","24":"ostatni-pozemky","25":"kancelare","26":"sklady","27":"vyrobni-prostory","28":"obchodni-prostory","29":"ubytovani","30":"restaurace","31":"zemedelske-objekty","38":"cinzovni-domy","49":"virtualni-kancelare","32":"ostatni-komercni-prostory","34":"garaze","52":"garazova-stani","50":"vinne-sklepy","51":"pudni-prostory","53":"mobilheimy","36":"jine-nemovitosti"},"category_sub_cb_detail":{"2":"1+kk","3":"1+1","4":"2+kk","5":"2+1","6":"3+kk","7":"3+1","8":"4+kk","9":"4+1","10":"5+kk","11":"5+1","12":"6-a-vice","16":"atypicky","47":"pokoj","37":"rodinny","39":"vila","43":"chalupa","33":"chata","35":"pamatka","40":"na-klic","44":"zemedelska-usedlost","19":"bydleni","18":"komercni","20":"pole","22":"louka","21":"les","46":"rybnik","48":"sady-vinice","23":"zahrada","24":"ostatni-pozemky","25":"kancelare","26":"sklad","27":"vyrobni-prostor","28":"obchodni-prostor","29":"ubytovani","30":"restaurace","31":"zemedelsky","38":"cinzovni-dum","49":"virtualni-kancelar","32":"ostatni-komercni-prostory","34":"garaz","52":"garazove-stani","50":"vinny-sklep","51":"pudni-prostor","53":"mobilni-domek","36":"jine-nemovitosti"}, "category_main_cb_detail":{"0":"vse","1":"byt","2":"dum","3":"pozemek","4":"komercni","5":"ostatni"}}',
            true
        );

        $parts = [];
        $parts[] = 'https://www.sreality.cz';
        $parts[] = 'detail';
        $parts[] = $codebooks['category_type_cb_detail'][$category_type_cb];
        $parts[] = $codebooks['category_main_cb_detail'][$category_main_cb];
        $parts[] = $codebooks['category_sub_cb_detail'][$category_sub_cb];
        $parts[] = $locality;
        $parts[] = $hashId;

        return implode('/', $parts);
    }
}
