<?php
declare(strict_types = 1);

namespace App\Servers;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class DownloadedPage
{
    /**
     * @ORM\Column(type="uuid")
     * @ORM\Id()
     * @var UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $content;

    public function __construct(
        string $content
    )
    {
        $this->id = Uuid::uuid4();
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
