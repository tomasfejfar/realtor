<?php

declare(strict_types = 1);

namespace App\Servers\SeveroceskeReality;

use App\Estate\Estate;
use App\Servers\DownloadedPage;
use App\Servers\DownloadedPageParser;
use Consistence\Type\ArrayType\ArrayType;
use Consistence\Type\ArrayType\KeyValuePair;
use Symfony\Component\DomCrawler\Crawler;

class SeveroceskeRealityParser implements DownloadedPageParser
{
    /**
     * @param DownloadedPage $downloadedPage
     * @return Estate[]
     */
    public function parse(DownloadedPage $downloadedPage): array
    {
        $crawler = new Crawler($downloadedPage->getContent());

        $container = $crawler->filter('.div_nemovitost_obal');
        $items = $crawler->filter('.div_nemovitost');

        $estates = ArrayType::mapByCallback(
            $items->getIterator()->getArrayCopy(), function (KeyValuePair $pair) {
            $crawler = new Crawler($pair->getValue());

            $linkToDetail = $crawler->filter('.nemo');

            $name = $linkToDetail->text();

            $detailLink = $linkToDetail->attr('href');
            $query = parse_url($detailLink, PHP_URL_QUERY);
            $params = [];
            parse_str($query, $params);
            $remoteId = $params['id'];

            $nameAndLocality = str_split($name, strpos($name, 'm2') + 2);
            $nameAndLocality = ArrayType::mapValuesByCallback(
                $nameAndLocality, function ($item) {
                return trim($item, ' ,');
            }
            );
            return new KeyValuePair(
                $pair->getKey(), new Estate(
                    $remoteId,
                    $nameAndLocality[0],
                    $nameAndLocality[1],
                    'https://severo.ceskereality.cz/?id=' . $remoteId
                )
            );
        });
        return $estates;
    }
}
