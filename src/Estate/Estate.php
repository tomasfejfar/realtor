<?php
declare(strict_types = 1);

namespace App\Estate;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 */
class Estate
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $remoteId;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $location;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $detailUrl;

    public function __construct(
        string $remoteId,
        string $name,
        string $location,
        string $detailUrl
    )
    {
        $this->id = Uuid::uuid4();
        $this->remoteId = $remoteId;
        $this->name = $name;
        $this->location = $location;
        $this->detailUrl = $detailUrl;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getRemoteId(): string
    {
        return $this->remoteId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function getDetailUrl(): string

    {
        return $this->detailUrl;
    }
}
