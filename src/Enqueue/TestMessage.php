<?php

declare(strict_types = 1);

namespace App\Enqueue;

use Interop\Queue\PsrContext;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestMessage extends Command
{
    /** @var PsrContext */
    private $context;

    public function __construct(
        PsrContext $context
    )
    {
        parent::__construct();
        $this->context = $context;
    }
    protected function configure()
    {
        $this->setName('q:t');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $this->context->createQueue('test_queue');
        $message = $this->context->createMessage('This is the message');
        $this->context->createProducer()->send($queue, $message);
    }
}
