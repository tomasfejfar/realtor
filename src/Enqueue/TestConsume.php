<?php

declare(strict_types = 1);

namespace App\Enqueue;

use Interop\Queue\PsrContext;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestConsume extends Command
{
    /** @var PsrContext */
    private $context;

    public function __construct(
        PsrContext $context
    )
    {
        parent::__construct();
        $this->context = $context;
    }
    protected function configure()
    {
        $this->setName('q:tc');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $this->context->createQueue('test_queue');
        $message = $this->context->createMessage('This is the message');
        $consumer = $this->context->createConsumer($queue);
        while ($message = $consumer->receive()) {
            $output->writeln($message->getBody());
            $consumer->acknowledge($message);
        }

        $output->writeln('No more messages');


    }
}
