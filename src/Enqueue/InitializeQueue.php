<?php

declare(strict_types = 1);

namespace App\Enqueue;

use Enqueue\Dbal\DbalContext;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitializeQueue extends Command
{
    /** @var DbalContext */
    private $context;

    public function __construct(
        DbalContext $context
    )
    {
        parent::__construct();
        $this->context = $context;
    }

    protected function configure()
    {
        $this->setName('queue:initializeDb');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->context->createDataBaseTable();
    }
}
