<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180102203947 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE estate (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', remote_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, location VARCHAR(255) NOT NULL, detail_url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE downloaded_page (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', content VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE enqueue');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE enqueue (id CHAR(36) NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:guid)\', published_at BIGINT NOT NULL, body LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, headers LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, properties LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, redelivered TINYINT(1) DEFAULT NULL, queue VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, priority SMALLINT NOT NULL, delayed_until INT DEFAULT NULL, time_to_live INT DEFAULT NULL, INDEX IDX_CFC35A68E0D4FDE1 (published_at), INDEX IDX_CFC35A687FFD7F63 (queue), INDEX IDX_CFC35A6862A6DC27 (priority), INDEX IDX_CFC35A681A065DF8 (delayed_until), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE estate');
        $this->addSql('DROP TABLE downloaded_page');
    }
}
