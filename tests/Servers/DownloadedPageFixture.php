<?php
declare(strict_types = 1);

namespace App\Servers;

class DownloadedPageFixture
{
    public static function getSrealityPage()
    {
        $contents = file_get_contents(__DIR__ . '/../../etc/responses/sreality.json');
        return new DownloadedPage($contents);
    }

    public static function getSeveroceskeRealityPage()
    {
        $contents = file_get_contents(__DIR__ . '/../../etc/responses/severoceskereality.html');
        return new DownloadedPage($contents);
    }
}
