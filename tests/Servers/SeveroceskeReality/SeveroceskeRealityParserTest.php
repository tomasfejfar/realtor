<?php

declare(strict_types = 1);

namespace App\Servers\SeveroceskeReality;

use App\Servers\DownloadedPageFixture;
use PHPUnit\Framework\TestCase;

class SeveroceskeRealityParserTest extends TestCase
{
    public function testParse(): void
    {
        $page = DownloadedPageFixture::getSeveroceskeRealityPage();
        $parser = new SeveroceskeRealityParser();
        
        $estates = $parser->parse($page);

        $this->assertCount(21, $estates);

        $x = $estates[0];
        $this->assertSame('Prodej bytu 4+kk, 88m2', $x->getName());
        $this->assertSame('Jablonec nad Nisou', $x->getLocation());
        $this->assertSame('FLV73313N317', $x->getRemoteId());
        $this->assertSame('https://severo.ceskereality.cz/?id=FLV73313N317', $x->getDetailUrl());

        $y = $estates[20];
        $this->assertSame('Prodej bytu 3+kk, 48m2', $y->getName());
        $this->assertSame('Jablonec nad Nisou', $y->getLocation());
        $this->assertSame('YFN12716614409', $y->getRemoteId());
        $this->assertSame('https://severo.ceskereality.cz/?id=YFN12716614409', $y->getDetailUrl());
    }
}
