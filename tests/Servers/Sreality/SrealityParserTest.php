<?php
declare(strict_types = 1);

namespace App\Servers\Sreality;

use App\Servers\DownloadedPageFixture;
use PHPUnit\Framework\TestCase;

class SrealityParserTest extends TestCase
{
    public function testParsing(): void
    {
        $page = DownloadedPageFixture::getSrealityPage();
        $parser = new SrealityParser();

        $estates = $parser->parse($page);

        $this->assertCount(2, $estates);

        $smallFlatOnPalackeho = $estates[0];

        $this->assertSame('Prodej bytu 2+1 55 m²', $smallFlatOnPalackeho->getName());
        $this->assertSame('Palackého, Jablonec nad Nisou - Mšeno nad Nisou', $smallFlatOnPalackeho->getLocation());
        $this->assertSame('2747822428', $smallFlatOnPalackeho->getRemoteId());
        $this->assertSame('https://www.sreality.cz/detail/prodej/byt/2+1/jablonec-nad-nisou-mseno-nad-nisou-palackeho/2747822428', $smallFlatOnPalackeho->getDetailUrl());

        $x= $estates[1];
        $this->assertSame('Prodej bytu 1+1 39 m²', $x->getName());
        $this->assertSame('Mechová, Jablonec nad Nisou - Mšeno nad Nisou', $x->getLocation());
        $this->assertSame('3138941276', $x->getRemoteId());
        $this->assertSame('https://www.sreality.cz/detail/prodej/byt/1+1/jablonec-nad-nisou-mseno-nad-nisou-mechova/3138941276', $x->getDetailUrl());
    }
}
